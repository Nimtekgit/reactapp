/**
 * In this file, we create a React component
 * which incorporates components providedby material-ui.
 */
import React, {Component} from 'react';
import {List, ListItem, makeSelectable} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import Paper from 'material-ui/Paper';
import ActionIcon from 'material-ui/svg-icons/content/add-circle-outline';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import RaisedButton from 'material-ui/RaisedButton';
import {deepOrange500} from 'material-ui/styles/colors';
import FlatButton from 'material-ui/FlatButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import xml from 'xml-loader';
var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();
import {
        white
        } from 'material-ui/styles/colors';

const avatar = {width: '100%', color: 'rgb(255,255,255)'};
const style = {background: "rgb(70,70,70)", width: 300, float: 'left', minHeight: 800};
const search = {background: 'rgb(100,100,100)', color: 'rgb(255,255,255)', width: '80%', margin: 'auto', marginLeft: '10%', paddingLeft: 10};
const divider = {background: "rgb(200,200,200)"};
const avatarname = {fontSize: 20};
const avatartxt = {marginLeft: 20, marginRight: 20, lineHeight: 1.5};
const style1 = {background: 'rgb(240,240,240)', width: 500, margin: 'auto'};
const divstyle = {float: 'left', paddingTop: 200, paddingLeft: 100, margin: 'auto'};
const iconStyles = {marginRight: 24, marginLeft: 20};
const addnum = {margin: 10, textAlign: 'right'};
const addtxt = {marginTop: 5, marginLeft: 5, color:'#000'};
const avatarDetails = {margin: 10};
const styleDetails = {height: 50};
const hidden = {marginRight: 24, marginLeft: 20, display: "none"};
const savebtn = {textAlign: 'center', marginTop:'5%', marginBottom:'5%', width:'40%',marginLeft:'auto', marginRight:'auto', display:'none'};
const styles = {
    container: {
        textAlign: 'left',
        marin: 5
    },
};
const muiTheme = getMuiTheme({
    palette: {
        accent1Color: deepOrange500,
    },
});

var filerdtx = [];
var telCount = 1;

var FilteredList = React.createClass({
  filterList: function(event){
    var updatedList = this.state.initialItems;
    var xmldata = require('xml-loader!./guestlist.xml');
    filerdtx.length = 0;
    updatedList = updatedList.filter(function(item, i){
        var filt = '';
       filt = item.toLowerCase().search(
        event.target.value.toLowerCase()) !== -1;
        if (filt) {
            filerdtx.push(xmldata.dataset.record[i]);
            return filt;
        }
    });
	var dtx = [filerdtx[0]
		]
        this.setState({data: dtx});
    this.setState({items: updatedList});
  },
  getInitialState: function(){
      //load xml file
      var xmldata = require('xml-loader!./guestlist.xml');
      var rows = [];
        xmldata.dataset.record.forEach(function (person, i) {
            var nameSurname = person.first_name + " " + person.last_name;
            rows.push(nameSurname)
            filerdtx[i] = person;
        });
     return {
       initialItems: rows
       ,
       items: [],
       data: [xmldata.dataset.record[0] //initial selected value
            ], selectedIndex: 0
     }
  },
  handleRequestChange: function(event, index) {
      //update Guest center block to the selected guest on list
        this.setState({
            selectedIndex: index
        });
        var dtx = [filerdtx[index]
		]
        this.setState({data: dtx});
		document.getElementById('1').style.display = 'none';
		document.getElementById('2').style.display = 'none';
       
    },
    validTel: function(e) {
        //validate Telephone
        var tel_pattern = /^[0-9() ]+$/;
        var field = e.target.value;
        var prefix = field.substr(0,1);
        if (field != '') {
            if (prefix === "(")
            {
               prefix = field.substr(0,2);
               field = field.substr(2);
            }
            else
               field = field.substr(1);
            if (!field.match(tel_pattern) || (prefix != "+" && prefix != "0" && prefix != "(0") || field.length<9)
            {
               alert('Invalid contact number');
               return false;
            }
            else
            {
               document.getElementById('btn').style.display = 'block';
                const users = this.state.data;
                users[0] = {Tel: "0731590660"};
                this.setState(users);
               return true;
            }
        }
    },
    validEmail: function(e) {
        //validate Email
        var email_pattern = /^[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}$/i;  
        var field = e.target.value;
        var atpos = field.indexOf("@");
        var dot1pos = field.indexOf(".", atpos);
        var dot2pos = field.lastIndexOf(".");
        if (field != '') {
            if (!field.match(email_pattern) || atpos < 1 || dot1pos - atpos < 3 || field.length - dot2pos < 3)
            {
               alert('Invalid email address');
               return false;
            }
            else
            {
               document.getElementById('btn').style.display = 'block';
               return true;
            }
        }
    },
    showTel: function(){
        //show extra tel feilds max 2
        if (telCount < 3) {
            document.getElementById(telCount).style.display = 'block';
                telCount++;
        }
    },
  componentWillMount: function(){
    this.setState({items: this.state.initialItems})
  },
  render: function(){
      
    return (
            <MuiThemeProvider muiTheme={muiTheme}>    
                <div style={styles.container}>
                <div className="InstantBox" style={style}>
                <TextField className="searchInput" onChange={this.filterList} floatingLabelText="Search guestlist"  style={search} underlineShow={false} />
                <br /><br />
                    <Divider style={divider} />
                    <ListSideBar RequestChange = {this.handleRequestChange} Index={this.state.selectedIndex} items={this.state.items}/>
                </div>
                {this.state.data.map((details, i) => (<GuestDetails key={i} showTel={this.showTel} validTel = {this.validTel} validEmail = {this.validEmail} data = {details} />))}
                </div>
              </MuiThemeProvider> 
                    
    );
  }
});

let SelectableList = makeSelectable(List);

class ListSideBar extends React.Component {

    render() {
        
        return (
            <div style={styles.container}>
                <SelectableList value={this.props.Index} onChange={this.props.RequestChange}>

                    {
                    this.props.items.map(function(person, i) {
                      return <ListItem value={i} key={i}
                                    leftAvatar={ < Avatar backgroundColor = {white} />}
                                        primaryText = {person}
                                        leftIcon = {<ContentInbox />}
                                        style = {avatarDetails}
                                 />
                    })
                   }
               </SelectableList>

            </div>

        );
    }

}

class GuestDetails extends React.Component {

    render() {
        
        return (
            <div style={divstyle}>
            
                <Paper zDepth={2} style={style1}>
                    <ListItem style={styleDetails}
                              disabled={true}
                              leftAvatar={
                        <Avatar
                            backgroundColor={white}
                            size={60}
                            style={avatarDetails}
                            >
                        </Avatar>
                        }
                        >
                        <div style={avatartxt}><span style={avatarname}>{this.props.data.first_name} {this.props.data.last_name}</span> 
                        <br /><i>{this.props.data.company}</i></div>
                    </ListItem>
                    <Divider />
                    <Subheader>Contact Details:</Subheader>
                    <TextField style={iconStyles} hintText="Email address" underlineShow={false}
                               floatingLabelText="Email address" onBlur={this.props.validEmail}
                               /><br />
                    <Divider />
                    
                    <TextField style={iconStyles} hintText="Mobile number" onBlur={this.props.validTel}
                               floatingLabelText="Mobile Number" defaultValue={this.props.data.Tel} underlineShow={false}
                               />
                    <Divider />
                    <div id="1" style={hidden} ><TextField   hintText="Mobile number" onBlur={this.props.validTel}
                               floatingLabelText="Mobile Number" underlineShow={false}
                               />
                    <Divider />
                    </div>
                    <div id="2" style={hidden} ><TextField   hintText="Mobile number" onBlur={this.props.validTel}
                               floatingLabelText="Mobile Number" underlineShow={false}
                               />
                    <Divider />
                    </div>
                    <div style={addnum}  >
                        <FlatButton label="Add Number" labelPosition="after" primary={true} icon={<ActionIcon  />} onTouchTap={this.props.showTel}
                                style={addtxt} 
                              />
                    </div>
                    <Divider />
                    
                    <div style={savebtn} id="btn">
                        <RaisedButton label="Save Details" style={avatar} primary={true} /></div>
                                <br />
                </Paper>
            </div>

        );
    }

}



            export default FilteredList;